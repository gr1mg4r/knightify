local action = require "necro.game.system.Action"
local ai = require "necro.game.enemy.ai.AI"
local attack = require "necro.game.character.Attack"
local beatmap = require "necro.audio.Beatmap"
local clientActionBuffer = require "necro.client.ClientActionBuffer"
local components = require "necro.game.data.Components"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local focus = require "necro.game.character.Focus"
local itemBan = require "necro.game.item.ItemBan"
local move = require "necro.game.system.Move"
local music = require "necro.audio.Music"
local objectEvents = require "necro.game.object.ObjectEvents"
local render = require "necro.render.Render"
local settings = require "necro.config.Settings"
local swipe = require "necro.game.system.Swipe"
local tile = require "necro.game.tile.Tile"
local turn = require "necro.cycles.Turn"

local color = require "system.utils.Color"
local ecs = require "system.game.Entities"
local enum = require "system.utils.Enum"

local draw = render.getBuffer(render.Buffer.OBJECT).draw
local opacity = color.opacity


local showArrowsEnum = enum.sequence {
	NEVER = 0,
	WHEN_IDLE = 1,
	ALWAYS = 2,
}

showArrows = settings.user.enum {
	enum = showArrowsEnum,
	default = showArrowsEnum.ALWAYS,
}

arrowOpacity = settings.user.percent {
	default = 0.9,
	step = 0.05,
}

----------
-- Main --
----------
components.register {
	knightify_knight = {},
	knightify_weaponForceParry = {},
	knightify_copyTargetMovementMap = {},
}

customEntities.extend {
	name = "knightify_Weapon",
	template = customEntities.template.item(),
	data = { slot = "weapon", flyaway = "Knight's Bite" },
	components = {
		weapon = {},
		weaponPattern = {
			pattern = {
				multiSwipe = "knightify_knight",
				tiles = {{
					offset = {2, -1},
					targetFlags = attack.Flag.DEFAULT,
				}},
			},
		},
		knightify_weaponForceParry = {},
		sprite = { texture = "gfx/necro/items/enemy/ability_swipe.png" },
		spriteSheet = { frameX = 3 },
	},
}

event.entitySchemaLoadPlayer.add("knightify", {order = "overrides", sequence = 42}, function (ev)
	ev.entity.knightify_knight = {}
	ev.entity.movable = { moveType = move.Flag.mask(move.Type.FLYING, move.Flag.TWEEN_BOUNCE_FAIL) }
	ev.entity.actionFilter = {
		ignoreActions = {
			[action.Special.SPELL_1] = true,
			[action.Special.SPELL_2] = true,
			[action.Special.THROW] = true,
			[action.Special.ITEM_2] = true,
		},
	}
	ev.entity.actionRemap = {}
	ev.entity.inputContextLayerDiagonal = {}
	ev.entity.remappedMovement = {
		map = {
			{  2, -1 }, {  1, -2 },
			{ -1, -2 }, { -2, -1 },
			{ -2,  1 }, { -1,  2 },
			{  1,  2 }, {  2,  1 },
		},
	}
	ev.entity.proximityReveal = { distance = 2.5 }
	ev.entity.visionRadial = {
		radius = 640,
		active = true,
	}
	ev.entity.hudHideSlots = {
		hidden = { [action.Special.THROW] = true },
	}
	ev.entity.facingMirrorX = {
		directions = { 1, 1, -1, -1, -1, -1, 1, 1 },
	}

	-- Initial inventory
	local items = ev.entity.initialInventory and ev.entity.initialInventory.items or {}
	for i = 1, #items do
		if items[i] == "ShovelBasic" then
			items[i] = "Pickaxe"
		elseif type(items[i]) == "string" and items[i]:find("Weapon") then
			items[i] = "knightify_Weapon"
		end
	end
	items[#items + 1] = "Food1"
	ev.entity.initialInventory = { items = items }

	-- Bans
	local bans = ev.entity.inventoryBannedItems and ev.entity.inventoryBannedItems.components or {}
	bans.itemBanWeaponlocked = itemBan.Type.FULL
	bans.shrineBanWeaponlocked = itemBan.Type.FULL
	bans.itemBanDiagonal = itemBan.Type.FULL
	bans.shrineBanDiamond = itemBan.Type.GENERATION
	ev.entity.inventoryBannedItems = { components = bans }

	local cursedSlots = ev.entity.inventoryCursedSlots and ev.entity.inventoryCursedSlots.slots or {}
	cursedSlots.spell = true
	cursedSlots.hud = true
	ev.entity.inventoryCursedSlots = { slots = cursedSlots }

	local enemyBans = ev.entity.enemyBans and ev.entity.enemyBans.types or {}
	enemyBans[#enemyBans + 1] = "Mole"
	ev.entity.enemyBans = { types = enemyBans }
end)

------------------
-- Enemy tweaks --
------------------
event.entitySchemaLoadNamedEnemy.add("knightifyClones", "clone", function (ev)
	ev.entity.movable = { moveType = move.Flag.mask(move.Type.FLYING, move.Flag.TWEEN_BOUNCE_FAIL) }
	ev.entity.knightify_copyTargetMovementMap = {}
end)

event.objectDirection.add("copyTargetMovementMap", {order = "remap", filter = {"knightify_copyTargetMovementMap", "target"}}, function (ev)
	local target = ecs.getEntityByID(ev.entity.target.entity)
	local map = target and target.remappedMovement and target.remappedMovement.map[ev.direction]
	if map then
		ev.dx = map[1]
		ev.dy = map[2]
		ev.x = ev.entity.position.x + ev.dx
		ev.y = ev.entity.position.y + ev.dy
	end
end)

event.aiAct.override("clone", ai.Type.CLONE, function (func, ev)
	if not (ev.target.knightify_knight and ev.target.character) then
		return func(ev)
	end
	if not ev.target.hasMoved or ev.target.hasMoved.value then
		ev.action = action.rotateDirection(ev.target.character.action, action.Rotation.MIRROR)
	end
end)

event.objectParry.override("prepareCounterAttack", 1, function (func, ev)
	if ev.weapon and ev.weapon.knightify_weaponForceParry then
		local ax, ay = ev.entity.position.x - ev.attacker.position.x, ev.entity.position.y - ev.attacker.position.y
		local direction = ax * ax > ay * ay and action.move(ax, 0) or action.move(0, ay)
		ev.entity.parryCounterAttack.active = true
		ev.entity.parryCounterAttack.direction = action.rotateDirection(direction, action.Rotation.MIRROR)
		ev.entity.character.canAct = false
		ev.entity.beatDelay.counter = 0
		if not ev.knockbackSuppressed then
			move.direction(ev.entity, direction, 1, move.getMoveType(ev.entity))
		end
		ev.knockback = 0
		ev.counterAttackReady = true
	else
		return func(ev)
	end
end)

-----------------
-- Item tweaks --
-----------------
event.holderMoveResult.override("performDashOnDig", 1, function (func, ev)
	if not ev.holder.knightify_knight then
		return func(ev)
	end
	if ev.result == action.Result.DIG and ev.entity.itemDashOnDig.active and not ev.didDashOnDig then
		ev.didDashOnDig = true
		move.absolute(ev.holder, ev.x, ev.y, move.getMoveType(ev.holder))
	end
	ev.entity.itemDashOnDig.active = false
end)

event.holderPerformAttack.override("performDashOnKill", 1, function (func, ev)
	if not ev.holder.knightify_knight then
		return func(ev)
	end
	if ev.entity.itemDashOnKill.active and ev.squareDistance == 0 then
		local map = ev.holder.remappedMovement and ev.holder.remappedMovement.map[ev.direction]
		if map then
			move.relative(ev.holder, map[1], map[2], move.getMoveType(ev.holder))
		else
			move.direction(ev.holder, ev.direction, 1, ev.entity.itemDashOnKill.moveType)
		end
	end
end)

event.holderTakeDamage.add("shieldTolerance", {order = "suppressFirst", sequence = -0.5, filter = "Sync_itemIncomingDamageImmunityDirectional"}, function (ev)
	if ev.holder.knightify_knight
		and not (ev.suppressed or ev.shielded or damage.Flag.check(ev.type, damage.Flag.BYPASS_ARMOR))
		and action.rotateDirection(ev.direction, action.Rotation.CCW_135) == ev.entity.facingDirection.direction
	then
		objectEvents.fire("applyDamageImmunity", ev.entity, ev)
	end
end)

-----------
-- Tells --
-----------
event.swipe.add("knight", "knightify_knight", function (ev)
	ev.dx = ev.swipe.x - ev.attacker.position.x
	ev.dy = ev.swipe.y - ev.attacker.position.y
	ev.swipe.angle = action.getAngle(action.move(ev.dx, ev.dy))
	ev.autoScaleX = false
	swipe.inherit(ev, "knight")
end)

local drawArgs = {
	texture = "mods/knightify/arrow.png",
	rect = { 0, 0, 24, 24 },
	texRect = { 0, 0, 24, 24 },
}
local function renderTell(direction, x, y)
	drawArgs.rect[1] = x * 24 - 12
	drawArgs.rect[2] = y * 24 - (tile.getInfo(x, y).renderFloor and 12 or 27)
	drawArgs.z = y * 24 + 24
	drawArgs.texRect[1] = (direction - 1) * 24
	draw(drawArgs)
end

event.render.add("knightifyTells", "tells", function (ev)
	if showArrows == showArrowsEnum.NEVER then
		return
	end

	local musicTime = music.getMusicTime()

	for entity in ecs.entitiesWithComponents {"knightify_knight", "remappedMovement", "controllable", "position"} do
		if focus.check(entity, focus.Flag.HUD) then
			local turnID = clientActionBuffer.getLatestTurnID(entity.controllable.playerID) or 0
			local animationTime = 1
			if turnID > 6 and showArrows == showArrowsEnum.WHEN_IDLE then
				animationTime = musicTime - turn.getTurnTimestamp(turnID) - 0.5
			end
			if animationTime > 0 then
				local beatFraction = beatmap.getForEntity(entity).getMusicBeatFraction()
				local alpha = math.min(animationTime, 0.5) * (1 + (1 - 2*beatFraction) ^ 4)
				drawArgs.color = color.opacity(alpha * arrowOpacity)
				local x, y = entity.position.x, entity.position.y
				for direction, offset in pairs(entity.remappedMovement.map) do
					renderTell(direction, x + offset[1], y + offset[2])
				end
			end
		end
	end
end)
